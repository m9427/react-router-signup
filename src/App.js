import React, { Component } from 'react';
import './App.css';
import logo from "./images/logo.png"
import { BrowserRouter, Switch, Route, Link } from 'react-router-dom';
import SignupPage from './container/SignupPage';
import Products from './container/Products';
import ProductDetails from './container/ProductDetails';


export class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      page: 'Home'
    }
  }

  Navbar = () => {
    return (
      <nav className="navbar navbar-expand-lg navbar-light my-navbar">
        <div className="container-fluid d-flex justify-content-between">
          <Link to="/"><div className="navbar-brand"><img src={logo} width="40px" height="40px" alt="company logo" /></div></Link>
          <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse" id="navbarSupportedContent">
            <ul className="navbar-nav me-auto mb-2 mb-lg-0">
              <li className="nav-item m-3">
                <Link to="/">Home</Link>
              </li>
              <li className="nav-item m-3">
                <Link to="/products">Products</Link>
              </li>
              <li className="nav-item m-3">
                <Link to="/signup">Sign-up</Link>
              </li>
            </ul>
          </div>

        </div>
      </nav>
    );
  }

  Home = () => {
    return (
      <div className="container col-lg-10 col-md-10 col-sm-12 p-0">
        <div className='d-flex flex-column align-items-md-end text-light strong-text'>
          We sell<br />
          <strong>everything</strong><br />
          <div className="btn btn-dark m-3 "><Link to="/products">Explore our products</Link></div>
          under <br />
          one roof.<br />
        </div>

      </div>
    );
  }

  render() {
    return (
      <BrowserRouter>
        <div className='container-fluid p-0 m-0 app-body'>
          <this.Navbar />
          <Switch>
            <Route path="/signup">
              <SignupPage />
            </Route>
            <Route path='/products/:id' component={ProductDetails} />
            <Route path='/products' exact component={Products} />
            <Route path='/' exact component={this.Home} />
          </Switch>
        </div>
      </BrowserRouter>
    );
  }
}

export default App

