
import React, { Component } from 'react'

export class ProductDetails extends Component {
  constructor(props) {
    super(props);

    this.state = {
      productInfo: [],
      fetched: false,
      error: false
    }
  }
  componentDidMount = () => {
    const id = this.props.match.params.id;
    this.getProductInfoById(id);
  }

  getProductInfoById = (inputId) => {
    fetch(`https://fakestoreapi.com/products/${inputId}`)
      .then(res => res.json())
      .then(data => {
        this.setState({ productInfo: data });
      }).then(() => {
        this.setState({ fetched: true })
      }).catch((e) => {
        this.setState({ error: true });
        console.log(e);
      });
  }

  renderProductDetails = () => {
    const gotData = this.state.productInfo;
    return (
      <div className='container col-lg-8 col-md-10 col-sm-12 p-0 mt-md-4'>
        <div className="card mb-3">
          <div className="row g-0">
            <div className="col-md-4 d-flex flex-column justify-content-md-center">
              <img src={gotData.image} className="img-fluid rounded-start" alt="Product_Image" />
            </div>
            <div className="col-md-8">
              <div className="card-body">
                <h5 className="card-title">{gotData.title}</h5>
                <p className="card-text">{gotData.description}</p>
                <h2> Price: &#36; {gotData.price}</h2>
                <div className="btn btn-primary m-2">Add to Cart</div>
                <p className="card-text"><small className="text-muted">Category: {gotData.category}</small></p>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }

  // render the spinner while the fetch is working in Background
  renderLoader = () => {
    return (
      <div className="container-fluid d-flex flex-column align-items-center mt-4">
        <div class="lds-roller"><div></div><div></div><div></div></div>
          <h1 className='text-light text-center'> Please wait... <br />Good things take time.</h1>
        </div>
        );
  }

  // Error meassage after failed API call
  failedAPI = () => {
    return (
        <div className='container-fluid'>
          <h1 className='text-warning text-center'> Awww.. Snap!!!<br /> Could not fetch the requested data. <br /> We have noted this issue and are working on the fix.</h1>
        </div>
        );
  }

  // No data from the API
  gotNoData = () => {
    return (
        <div className='container-fluid'>
          <h1 className='text-center text-danger'> OOPS...<br /> There is no data for the page requested.</h1>
        </div>
        );
  }

        render() {

    return (
        (!this.state.fetched && !this.state.error) ?
        this.renderLoader()
        :
        (this.state.fetched && this.state.productInfo) ?
        this.renderProductDetails()
        :
        (this.state.error) ?
        this.failedAPI()
        :
        this.gotNoData()
        );
  }
}

        export default ProductDetails