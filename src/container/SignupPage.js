import React, { Component } from 'react'
import validator from 'validator';

export class SignupPage extends Component {

    constructor(props) {
        super(props)

        this.state = {
            firstName: '',
            lastName: '',
            email: '',
            age: '',
            gender: '',
            role: '',
            password: '',
            passwordMatch: '',
            boxChecked: false,
            firstNameError: '',
            lastNameError: '',
            emailError: '',
            genderError: '',
            termsError: '',
            ageError: '',
            roleError: '',
            passed: true,
            proceed: false
        }
    }

    handleFirstName = (event) => {
        const firstNameValue = event.target.value;
        this.setState({
            firstName: firstNameValue
        });
    }
    handleLastName = (event) => {
        const lastNameValue = event.target.value;
        this.setState({
            lastName: lastNameValue
        });
    }
    handleEmail = (event) => {
        const emailValue = event.target.value;
        this.setState({
            email: emailValue
        });
    }
    handleGender = (event) => {
        const genderValue = event.target.value;
        this.setState({
            gender: genderValue
        });
    }
    handleChecked = (event) => {
        const checkedState = event.target.checked;
        this.setState({
            boxChecked: checkedState
        });
    }
    handleAge = (event) => {
        const ageValue = event.target.value;
        this.setState({
            age: ageValue
        });
    }
    handleRole = (event) => {
        const roleValue = event.target.value;
        this.setState({
            role: roleValue
        });
    }
    handlePassword = (event) => {
        const passwordValue = event.target.value;
        this.setState({
            password: passwordValue
        });
    }
    handlePasswordMatch = (event) => {
        const passwordMatchValue = event.target.value;
        this.setState({
            passwordMatch: passwordMatchValue
        });
    }

    validateName = (inputName) => {
        if (inputName !== "" && validator.isAlpha(inputName)) {
            return true;
        }
        return false;
    }
    validateEmail = (inputText) => {
        if (inputText !== "" && validator.isEmail(inputText)) {
            return true;
        }
        return false;
    }
    validateAge(inputAge) {
        if (inputAge !== "" && validator.isNumeric(inputAge)) {
            return true;
        }
        return false;
    }
    validatePassword(inputPassword) {
        if (inputPassword !== "" && validator.isStrongPassword(inputPassword)) {
            return true;
        }
        return false;
    }


    submitHandler = (event) => {
        console.log(this.state);
        event.preventDefault();

        // validate first name
        if (!this.validateName(this.state.firstName)) {
            this.setState({
                firstNameError: 'yes',
                passed: false
            });
        } else {
            this.setState({
                firstNameError: 'no'
            });
        }

        // Validate Last Name
        if (!this.validateName(this.state.lastName)) {
            this.setState({
                lastNameError: 'yes',
                passed: false
            });
        } else {
            this.setState({
                lastNameError: 'no'
            });
        }

        // Validate email
        if (!this.validateEmail(this.state.email)) {
            this.setState({
                emailError: 'yes',
                passed: false
            });
        } else {
            this.setState({
                emailError: 'no'
            });
        }

        // Validate Gender
        if (this.state.gender === "") {
            this.setState({
                genderError: 'yes',
                passed: false
            });
        } else {
            this.setState({
                genderError: 'no'
            });
        }

        // Validate terms and condition
        if (this.state.boxChecked === false) {
            this.setState({
                termsError: 'yes',
                passed: false
            });
        } else {
            this.setState({
                termsError: 'no'
            });
        }

        // Validate age
        if (!this.validateAge(this.state.age)) {
            this.setState({
                ageError: 'yes',
                passed: false
            });
        } else {
            this.setState({
                ageError: 'no'
            });
        }

        // Validate role
        if (this.state.role === "") {
            this.setState({
                roleError: 'yes',
                passed: false
            });
        } else {
            this.setState({
                roleError: 'no'
            });
        }

        // Validate Password
        if (!this.validatePassword(this.state.password)) {
            this.setState({
                passwordError: 'yes',
                passed: false
            });
        } else {
            this.setState({
                passwordError: 'no'
            });
        }

        // If all okay then set the proceed state to display the success message
        if (this.state.passed && this.state.boxChecked && this.state.passwordMatch === this.state.password) {
            this.setState({ proceed: true }, () => {
                console.log('Successful Signup!');
            })
        }

    }

    render() {
        return (
            <div className="container col-lg-6 col-md-8 col-sm-12 mt-lg-2 p-2 rounded-3 form-body">
                <h2 className='text-center'>SIGN-UP</h2>
                <div className="row">
                    <div className="col">
                        <form className="row g-3 " noValidate>

                            <div className="col-md-6 d-flex flex-column">
                                <label htmlFor="firstname" className="form-label-sm">First name</label>
                                <input type="text" className="form-control-sm " id="firstname" onChange={this.handleFirstName} />
                                {(this.state.firstNameError === "yes") ?
                                    <div className="text-danger">
                                        <small>Please check the first name.</small>
                                    </div>
                                    :
                                    null
                                }
                            </div>

                            <div className="col-md-6 d-flex flex-column">
                                <label htmlFor="lastname" className="form-label-sm">Last name</label>
                                <input type="text" className="form-control-sm" id="lastname" onChange={this.handleLastName} />
                                {(this.state.lastNameError === "yes") ?
                                    <div className="text-danger">
                                        <small>Please check the last name.</small>
                                    </div>

                                    :
                                    null

                                }
                            </div>

                            <div className="col-md-6 d-flex flex-column">
                                <label htmlFor="age" className="form-label-sm">Age</label>
                                <input type="text" className="form-control-sm" id="age" onChange={this.handleAge} />
                                {(this.state.ageError === "yes") ?
                                    <div className="text-danger">
                                        <small>Age should be in numbers</small>
                                    </div>

                                    :
                                    null

                                }
                            </div>

                            <div className="col-md-6 d-flex flex-column">
                                <label htmlFor="mail" className="form-label-sm">e-mail</label>
                                <input type="text" className="form-control-sm" id="mail" onChange={this.handleEmail} />
                                {(this.state.emailError === "yes") ?
                                    <div className="text-danger">
                                        <small>Please check the e-mail.</small>
                                    </div>

                                    :
                                    null

                                }
                            </div>

                            <div className="col-md-6">
                                Gender
                                <div className="form-check">
                                    <input className="form-check-input" type="radio" name="gender" id="male" value="male" onChange={this.handleGender} />
                                    <label className="form-check-label" htmlFor="male">
                                        Male
                                    </label>
                                </div>
                                <div className="form-check">
                                    <input className="form-check-input" type="radio" name="gender" id="female" value="female" onChange={this.handleGender} />
                                    <label className="form-check-label" htmlFor="female">
                                        Female
                                    </label>
                                </div>
                                <div className="form-check">
                                    <input className="form-check-input" type="radio" name="gender" id="other" value="other" onChange={this.handleGender} />
                                    <label className="form-check-label" htmlFor="other">
                                        Other
                                    </label>
                                </div>
                                {(this.state.genderError === "yes") ?
                                    <div className="text-danger">
                                        <small>Please select the appropriate gender.</small>
                                    </div>

                                    :
                                    null
                                }
                            </div>


                            <div className="col-md-6">
                                Role
                                <div className="form-check">
                                    <input className="form-check-input" type="radio" name="role" id="developer" value="developer" onChange={this.handleRole} />
                                    <label className="form-check-label" htmlFor="developer">
                                        Developer
                                    </label>
                                </div>
                                <div className="form-check">
                                    <input className="form-check-input" type="radio" name="role" id="seniordeveloper" value="seniordeveloper" onChange={this.handleRole} />
                                    <label className="form-check-label" htmlFor="seniordeveloper">
                                        Senior Developer
                                    </label>
                                </div>
                                <div className="form-check">
                                    <input className="form-check-input" type="radio" name="role" id="leadengineer" value="leadengineer" onChange={this.handleRole} />
                                    <label className="form-check-label" htmlFor="leadengineer">
                                        Lead Engineer
                                    </label>
                                </div>
                                <div className="form-check">
                                    <input className="form-check-input" type="radio" name="role" id="cto" value="cto" onChange={this.handleRole} />
                                    <label className="form-check-label" htmlFor="cto">
                                        CTO
                                    </label>
                                </div>
                                {(this.state.roleError === "yes") ?
                                    <div className="text-danger">
                                        <small> Please select the applicable role.</small>
                                    </div>
                                    :
                                    null
                                }
                            </div>


                            <div className="col-md-6 d-flex flex-column">
                                <label htmlFor="password" className="form-label-sm">Enter your desired password</label>
                                <input type="password" className="form-control-sm" id="password" onChange={this.handlePassword} />
                                <p className='text-primary password-text'>(Min 8 digits with min 1 Uppercase, 1 Lowercase, 1 Number and 1 special symbol)</p>
                                {(this.state.passwordError === "yes") ?
                                    <div className="text-danger">
                                        <small>Password doesn't satisfy the requirement.</small>
                                    </div>

                                    :
                                    null

                                }
                            </div>

                            <div className="col-md-6 d-flex flex-column">
                                <label htmlFor="matchpassword" className="form-label-sm">Re-enter the password</label>
                                <input type="password" className="form-control-sm" id="matchpassword" onChange={this.handlePasswordMatch} />
                                {(this.state.passwordMatch !== "" & this.state.passwordMatch === this.state.password) ?
                                    <div className="text-success">
                                        <small> Password matched!</small>
                                    </div>
                                    :
                                    (this.state.passwordMatch === "") ?
                                        null
                                        :
                                        <div className="text-danger">
                                            <small>Password doesn't match.</small>
                                        </div>
                                }
                            </div>

                            <div className="col-12 d-flex justify-content-center">
                                <div className="form-check">
                                    <input className="form-check-input" type="checkbox" value="" id="invalidCheck" onChange={this.handleChecked} />
                                    <label className="form-check-label" htmlFor="invalidCheck">
                                        <small> Agree to terms and conditions </small>
                                    </label>
                                    {(this.state.termsError === "yes") ?
                                        <div className="text-danger">
                                            <small>You must agree before submitting.</small>
                                        </div>
                                        :
                                        null
                                    }
                                </div>
                            </div>

                            <div className="col-12 d-flex justify-content-center">
                                <button className="btn" style={{ "backgroundColor": "#493794", "color": "white" }} type="submit" onClick={this.submitHandler}>Sign-up</button>
                            </div>

                        </form>
                        {
                            (this.state.proceed) ?
                                <div className=' container text-center text-success'>
                                    Hi {this.state.firstName}! You have successfully signed up.<br /> Enjoy Shopping!!!
                                </div>
                                :
                                null
                        }
                    </div>


                </div>
            </div>
        )
    }
}

export default SignupPage