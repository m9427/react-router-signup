import React, { Component } from 'react';
import { Link } from 'react-router-dom';

export class Products extends Component {
    constructor(props) {
        super(props);

        this.state = {
            productData: [],
            fetched: false,
            error: false
        }
    }

    componentDidMount = () => {
        fetch('https://fakestoreapi.com/products')
            .then(res => res.json())
            .then(data => {
                this.setState({ productData: data });
            }).then(() => {
                this.setState({ fetched: true })
            }).catch((e) => {
                this.setState({ error: true });
                console.log(e);
            });
    }

    // Create product cards
    createProductCards = (dataArray) => {
        return dataArray.map((item) => {
            return (
                <div className="card col-lg-3 col-md-4 col-sm-11" key={item.id}>
                    <div className="card-header">
                        {item.category}
                    </div>
                    <img src={item.image} className="card-img-top" alt="Product_Image" />
                    <div className="card-body d-flex flex-column justify-content-md-between">
                        <h5 className="card-title">{item.title}</h5>
                        <h3> Price : &#36; {item.price}</h3>
                        <p className="card-text">Ratings: {item.rating.rate} <br />
                            Total rating count: {item.rating.count}
                        </p>
                        <Link to={`/products/${item.id}`} className="btn btn-primary pointer-setter">View Details</Link>
                    </div>
                </div>
            );
        });
    }

    // render cards after successfull data fetch
    renderProducts = () => {
        const myCards = this.createProductCards(this.state.productData);
        return (
            <div className='container-fluid'>
                <div className="row">
                    <div className="col-lg-12 d-flex justify-content-between"></div>
                    {myCards}
                </div>
            </div>
        );
    }

    // render the spinner while the fetch is working in Background
    renderLoader = () => {
        return (
            <div className="container-fluid d-flex flex-column align-items-center mt-4">
                <div class="lds-roller"><div></div><div></div><div></div></div>
                <h1 className='text-light text-center'> Please wait... <br />Good things take time.</h1>
            </div>
        );
    }

    // Error meassage after failed API call
    failedAPI = () => {
        return (
            <div className='container-fluid'>
                <h1 className='text-warning text-center'> <strong>Awww.. Snap!!!</strong><br /> Could not fetch the requested data. <br /><small> We have noted this issue and are working on the fix.</small></h1>
            </div>
        );
    }

    // No data from the API
    gotNoData = () => {
        return (
            <div className='container-fluid'>
                <h1 className='text-center text-danger'> OOPS...<br /> There is no data for the page requested.</h1>
            </div>
        );
    }

    render() {
        // console.log(this.state);

        return (
            (!this.state.fetched && !this.state.error) ?
                this.renderLoader()
                :
                (this.state.fetched && this.state.productData) ?
                    this.renderProducts()
                    :
                    (this.state.error) ?
                        this.failedAPI()
                        :
                        this.gotNoData()

        );
    }
}

export default Products